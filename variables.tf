variable "name_prefix" {
  description = "Friendly name prefix used when naming resources."
  default     = "tfcb-test"
}

variable "cf_stack_name" {
  description = "Name to assign to CF VPC Stack."
  default     = "test-vpc-172"
}

variable "vpc_cidr" {
  description = "CIDR to assign to VPC."
  default     = "172.16.0.0/16"
}


