# deploy-cf-vpc

> CloudFormation Example 1 - Deploy a CloudFormation VPC stack using Terraform.

## Dependencies

- Deploy the resources in this example before running the `consume-cf-vpc` example.

## Variables

- `name_prefix` - Friendly name prefix used when naming resources.
- `cf_stack_name` - Name to assign to CF VPC Stack.
  - This value used in this repo must be the same as `consume-cf-vpc`
  - default: `test-vpc-172`
- `vpc_cidr` - CIDR to assign to VPC.
  - default: `172.16.0.0/16`

## Notes

- If `vpc_cidr` value is changed
  - Must also change `subnet_cidr` in `consume-cf-vpc` repo to a valid subnet CIDR of the new `vpc_cidr` value.
- `cf_stack_name` must have the same value in both repos `deploy-cf-vpc` and `consume-cf-vpc`

## Tear Down

- Do NOT run a destroy plan on this repo until the infrastructure in the `consume-cf-vpc` has been successfully destroyed.
