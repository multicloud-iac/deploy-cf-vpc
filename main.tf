# Provision VPC using CloudFormation template

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.46.0"
    }
  }
  required_version = ">= 0.13"
}

# INITIALIZE AWS PROVIDER
provider "aws" {
  region = "us-west-2"
}

# DEPLOY VPC VIA CLOUDFORMATION STACK
resource "aws_cloudformation_stack" "network" {
  name = var.cf_stack_name

  parameters = {
    VPCCidr = var.vpc_cidr
  }

  tags = {
    Name    = "${var.name_prefix}-vpc-${join("", regex("^(\\d+)\\.\\d+\\.\\d+\\..*", var.vpc_cidr))}"
    Project = var.name_prefix
  }

  template_body = <<STACK
{
  "Parameters" : {
    "VPCCidr" : {
      "Type" : "String",
      "Default" : "10.0.0.0/16",
      "Description" : "Enter the CIDR block for the VPC. Default is 10.0.0.0/16."
    }
  },
  "Resources" : {
    "vpc10": {
      "Type" : "AWS::EC2::VPC",
      "Properties" : {
        "CidrBlock" : { "Ref" : "VPCCidr" },
        "EnableDnsHostnames" : true,
        "EnableDnsSupport" : true
      }
    }
  },
  "Outputs" : {
    "VPCId" : {
      "Description" : "Id of the newly created VPC",
      "Value" : { "Ref" : "vpc10" }
    }
  }
}
STACK
}

# DISPLAY VPC ID
output "vpc_id" {
  value = aws_cloudformation_stack.network.outputs.VPCId
}
